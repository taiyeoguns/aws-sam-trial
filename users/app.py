import httpx
from aws_lambda_powertools import Logger, Metrics, Tracer
from aws_lambda_powertools.event_handler.api_gateway import ApiGatewayResolver
from aws_lambda_powertools.logging import correlation_paths
from aws_lambda_powertools.metrics import MetricUnit

logger = Logger(service="APP")
tracer = Tracer(service="APP")
metrics = Metrics(namespace="MyApp", service="APP")
app = ApiGatewayResolver()

client = httpx.Client(base_url="https://jsonplaceholder.typicode.com")


@app.get("/users/<name>")
@tracer.capture_method
def users_name(name):
    tracer.put_annotation(key="User", value=name)
    logger.info(f"Request from {name} received")
    metrics.add_metric(name="SuccessfulGreetings", unit=MetricUnit.Count, value=1)
    return {"message": f"hello {name}!"}


@app.get("/users")
@tracer.capture_method
def users():
    tracer.put_annotation(key="User", value="all")
    logger.info("Request for users received")
    metrics.add_metric(name="SuccessfulGreetings", unit=MetricUnit.Count, value=1)

    message = ""

    try:
        response = client.get("/users")
        response.raise_for_status()
        message = response.json()
    except httpx.HTTPError as err:
        message = str(err)

    return {"message": message}


@tracer.capture_lambda_handler
@logger.inject_lambda_context(
    correlation_id_path=correlation_paths.API_GATEWAY_REST, log_event=True
)
@metrics.log_metrics(capture_cold_start_metric=True)
def lambda_handler(event, context):
    try:
        return app.resolve(event, context)
    except Exception as e:
        logger.exception(e)
        raise
