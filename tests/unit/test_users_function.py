import json

import httpx
from users import app


def test_lambda_handler_users_path(apigw_users_event, lambda_context, respx_mock):
    expected_message = {
        "id": 1,
        "name": "Test User",
        "username": "testuser",
        "email": "test@email.com",
        "address": {
            "street": "Street",
            "suite": "Suite",
            "city": "City",
            "zipcode": "1234",
            "geo": {"lat": "-37.3159", "lng": "81.1496"},
        },
        "phone": "1-234-567-8900",
        "website": "test.com",
        "company": {
            "name": "Company",
            "catchPhrase": "Multi-layered client-server neural-net",
            "bs": "harness real-time e-markets",
        },
    }
    respx_mock.get("https://jsonplaceholder.typicode.com/users").mock(
        return_value=httpx.Response(status_code=200, json=expected_message)
    )

    expected = json.dumps({"message": expected_message}, separators=(",", ":"))

    ret = app.lambda_handler(apigw_users_event, lambda_context)

    assert ret["statusCode"] == 200
    assert ret["body"] == expected


def test_lambda_handler_users_you_path(apigw_users_name_event, lambda_context):
    ret = app.lambda_handler(apigw_users_name_event, lambda_context)
    expected = json.dumps({"message": "hello you!"}, separators=(",", ":"))

    assert ret["statusCode"] == 200
    assert ret["body"] == expected
