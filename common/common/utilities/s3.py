from pathlib import Path
from typing import Any

import boto3
from botocore.exceptions import ClientError
from common.config import PRESIGNED_URL_EXPIRATION_TIME, S3_BUCKET_NAME

session = boto3.session.Session()
s3 = session.client("s3")


def folder_exists_and_not_empty(
    bucket: str,
    path: str,
    max_keys: int = 20,
) -> bool:
    if not path.endswith("/"):
        path += "/"
    resp = s3.list_objects(
        Bucket=bucket,
        Prefix=path,
        Delimiter="/",
        MaxKeys=max_keys,
    )
    return "Contents" in resp


def get_object_from_bucket(bucket: str, path: str) -> Any:
    path = path.rstrip("/")

    try:
        resp = s3.get_object(Bucket=bucket, Key=path)
    except ClientError as err:
        raise ValueError(f"Object '{path}' not found in bucket '{bucket}'.") from err

    return resp["Body"].read()


def get_file_from_repo(repository: str, path: str) -> Any:
    file_path = f"{repository}/{path}"
    try:
        return get_object_from_bucket(bucket=S3_BUCKET_NAME, path=file_path)
    except ValueError as err:
        raise ValueError(
            f"File with path '{path}' not found in repository '{repository}'",
        ) from err


def generate_presigned_url(
    path: str,
    bucket: str = S3_BUCKET_NAME,
    expiration: int = PRESIGNED_URL_EXPIRATION_TIME,
) -> str:
    try:
        return s3.generate_presigned_url(
            "get_object",
            Params={"Bucket": bucket, "Key": path},
            ExpiresIn=expiration,
        )

    except ClientError as err:
        raise ValueError(
            f"Unable to create presigned url for '{path}' in bucket '{bucket}'",
        ) from err


def get_asset_path(repository, path, asset_source):
    src_asset_path = ((Path.home() / path).parent / asset_source).resolve()
    original_path = (
        (Path.home() / path).parents[len(path.lstrip("/").split("/")) - 1].resolve()
    )
    relative_path = src_asset_path.relative_to(original_path)
    asset_path = str(relative_path.as_posix())

    return f"{repository.rstrip('/')}/{asset_path}"
