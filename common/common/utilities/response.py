import json
from http import HTTPStatus
from types import MappingProxyType
from typing import Optional, Union

DEFAULT_CORS_HEADERS = MappingProxyType(
    {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT",
        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,Accept,traceparent,tracestate",
    }
)


def http_response(
    status_code: Union[HTTPStatus, int],
    body: dict,
    headers: Optional[dict] = None,
) -> dict:
    """
    Return a standard Lambda response.

    :param status_code: A HTTPStatus (preferred) or literal status code
    :param body: Dictionary body that is dumped to JSON
    :param headers: Dictionary of HTTPS headers
    :return: Lambda response payload
    """
    headers = headers or {}

    if not isinstance(status_code, int):
        status_code = int(status_code)

    return {
        "statusCode": status_code,
        "body": json.dumps(body),
        "headers": {**DEFAULT_CORS_HEADERS, **headers},
    }
